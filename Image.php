<?php

require_once('DB.php');

class Image
{

    public $database;

    public function __construct()
    {
        $DB = new DB();
        $this->database = $DB->database();
    }

    public function __destruct()
    {
        $this->database->close();
    }

    public function read()
    {
        $query = $this->database
            ->query("SELECT * FROM images");

        return $query->fetch_all(MYSQLI_ASSOC);
    }

    public function create($filename, $path)
    {
        $query = $this->database
            ->query(
                "INSERT INTO images (id, filename, path, created_at, updated_at) 
                VALUES (
                    NULL,
                    '{$this->database->real_escape_string($filename)}', 
                    '{$this->database->real_escape_string($path)}',
                    current_timestamp(),
                    current_timestamp()
                )"
            );

        return $query;
    }

    public function update($id, $filename = "", $path = "")
    {
        $query = $this->database
            ->query(
                "UPDATE images SET
                    filename = '{$this->database->real_escape_string($filename)}', 
                    path = '{$this->database->real_escape_string($path)}' 
                WHERE id = {$this->database->real_escape_string($id)}"
            );

        return $query;
    }

    public function delete($id)
    {
        $query = $this->database
            ->query(
                "DELETE FROM images WHERE id = {$this->database->real_escape_string($id)}"
            );

        return $query;
    }

    public function id()
    {
        return $this->database->insert_id;
    }

    public function getImageById($id)
    {
        $query = $this->database
            ->query("SELECT * FROM images WHERE id = {$id}");

        return $query->fetch_assoc();
    }
}
