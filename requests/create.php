<?php
error_reporting(E_ALL);
ini_set("display_errors", "on");

require_once("../Image.php");

$filename = $_FILES['image']['name'];
$location = "uploads/" . $filename;
$validation = true;
$imageFileType = pathinfo($location, PATHINFO_EXTENSION);
$validExtensions = array("jpeg", "png", "svg", "gif", "jpg");

if (!in_array(strtolower($imageFileType), $validExtensions)) {
    $validation = false;
} else {
    $image = new Image();
    if ($image->create($filename, $location)) {
        if (move_uploaded_file($_FILES['image']['tmp_name'], $location)) {
            header('Content-type: application/json');
            http_response_code(201);
            return json_encode([
                "image" => $image->getImageById($image->id())
            ]);
        }
    } else {
        header('Content-type: application/json');
        http_response_code(500);
        return json_encode(["message" => "Image failed uploading."]);
    }
}
