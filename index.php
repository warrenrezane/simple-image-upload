<?php
require_once("Image.php");
$image = new Image();
$images = $image->read();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="./style.css">
  <title>Document</title>
</head>

<body>
  <div class="table-body">
    <div class="table-section">
      <button class="btn upload-btn" id="btn_open_modal" style="margin: 15px 0 15px 0;">Upload</button>
      <table id="table">
        <tr class="header-row">
          <!-- <th class="header-item items">Title</th> -->
          <th class="header-item items">Thumbnail</th>
          <th class="header-item items">Filename</th>
          <th class="header-item items">Date Added</th>
          <th class="header-item items" colspan="2">Actions</th>
        </tr>

        <?php
        foreach ($images as $data) {
        ?>
          <tr class="table-rows" data-id="<?php echo $data['id'] ?>">
            <td class="items">
              <img src="<?php echo $data['path'] ?>" alt="" width="70">
            </td>
            <td class="items"><?php echo $data['filename'] ?></td>
            <td class="items"><?php echo date('M d, Y', strtotime($data['created_at'])) ?></td>
            <td class="items">
              <button class="btn edit-btn">Edit</button>
            </td>
            <td class="items"><button class="btn delete-btn">Delete</button></td>
          </tr>
        <?php } ?>
      </table>
    </div>
  </div>

  <div class="modal-body" id="image_upload_modal">
    <div id="uploadArea" class="upload-area">
      <!-- Header -->
      <div class="upload-area__header">
        <h1 class="upload-area__title">Upload your file</h1>
        <p class="upload-area__paragraph">
          File should be an image
          <strong class="upload-area__tooltip">
            Like
            <span class="upload-area__tooltip-data"></span> <!-- Data Will be Comes From Js -->
          </strong>
        </p>
      </div>
      <!-- End Header -->

      <!-- Drop Zoon -->
      <div id="dropZoon" class="upload-area__drop-zoon drop-zoon">
        <span class="drop-zoon__icon">
          <i class='bx bxs-file-image'></i>
        </span>
        <p class="drop-zoon__paragraph">Drop your file here or Click to browse</p>
        <span id="loadingText" class="drop-zoon__loading-text">Please Wait</span>
        <img src="" alt="Preview Image" id="previewImage" class="drop-zoon__preview-image" draggable="false">
        <input type="file" id="fileInput" class="drop-zoon__file-input" accept="image/*">
      </div>
      <!-- End Drop Zoon -->

      <!-- File Details -->
      <div id="fileDetails" class="upload-area__file-details file-details">
        <h3 class="file-details__title">Uploaded File</h3>

        <div id="uploadedFile" class="uploaded-file">
          <div class="uploaded-file__icon-container">
            <i class='bx bxs-file-blank uploaded-file__icon'></i>
            <span class="uploaded-file__icon-text"></span> <!-- Data Will be Comes From Js -->
          </div>

          <div id="uploadedFileInfo" class="uploaded-file__info">
            <span class="uploaded-file__name">Project 1</span>
            <span class="uploaded-file__counter">0%</span>
          </div>
        </div>
      </div>
      <button class="btn close-btn" id="btn_close_modal">Close</button>
      <br>
      <div style="margin-top: 20px;">
        <h4 id="success-message" style="color: green;" hidden>✅ File Successfully Uploaded!</h4>
        <h4 id="failed-message" style="color: red;" hidden>❌ File Failed Uploading</h4>
      </div>

      <!-- End File Details -->
    </div>
  </div>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="image_upload.js"></script>
  <script>
    // Get the modal
    var modal = document.getElementById("image_upload_modal");

    // Get the button that opens the modal
    var btn = document.getElementById("btn_open_modal");

    var close = document.getElementById("btn_close_modal");

    // When the user clicks on the button, open the modal
    btn.onclick = function() {
      modal.style.display = "block";
    }

    close.onclick = function() {
      modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
      if (event.target == modal) {
        modal.style.display = "none";
      }
    }
  </script>
</body>

</html>