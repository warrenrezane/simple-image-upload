<?php

define('DB_HOST', '127.0.0.1');
define('DB_PORT', 3306);
define('DB_DATABASE', 'simple_image_upload');
define('DB_USER', 'root');
define('DB_PASSWORD', '');

class DB
{
    public $mysql;

    public function __construct()
    {
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
        $this->mysql = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE, DB_PORT);
    }

    public function database()
    {
        return $this->mysql;
    }

    public function close()
    {
        return $this->mysql->close();
    }
}
