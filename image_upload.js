const uploadArea = document.querySelector("#uploadArea");
const dropZoon = document.querySelector("#dropZoon");
const loadingText = document.querySelector("#loadingText");
const fileInput = document.querySelector("#fileInput");
const previewImage = document.querySelector("#previewImage");
const fileDetails = document.querySelector("#fileDetails");
const uploadedFile = document.querySelector("#uploadedFile");
const uploadedFileInfo = document.querySelector("#uploadedFileInfo");
const uploadedFileName = document.querySelector(".uploaded-file__name");

const success_message = document.querySelector("#success-message");
const failed_message = document.querySelector("#failed-message");

const uploadedFileIconText = document.querySelector(
  ".uploaded-file__icon-text"
);

const uploadedFileCounter = document.querySelector(".uploaded-file__counter");
const toolTipData = document.querySelector(".upload-area__tooltip-data");

const imagesTypes = ["jpeg", "png", "svg", "gif", "jpg"];

toolTipData.innerHTML = [...imagesTypes].join(", .");

dropZoon.addEventListener("dragover", function (event) {
  event.preventDefault();
  dropZoon.classList.add("drop-zoon--over");
});

dropZoon.addEventListener("dragleave", function (event) {
  dropZoon.classList.remove("drop-zoon--over");
});

dropZoon.addEventListener("drop", function (event) {
  event.preventDefault();

  dropZoon.classList.remove("drop-zoon--over");

  const file = event.dataTransfer.files[0];
  uploadFile(file);
});

dropZoon.addEventListener("click", function (event) {
  fileInput.click();
});

fileInput.addEventListener("change", function (event) {
  const file = event.target.files[0];
  const form_data = new FormData();
  form_data.append("image", file);

  $.ajax({
    url: "requests/create.php",
    method: "POST",
    data: form_data,
    contentType: false,
    processData: false,
    success: function (response) {
      console.log(response);
    },
  });

  uploadFile(file);
});

function uploadFile(file) {
  const fileReader = new FileReader();
  const fileType = file.type;
  const fileSize = file.size;

  if (fileValidate(fileType, fileSize)) {
    dropZoon.classList.add("drop-zoon--Uploaded");

    loadingText.style.display = "block";
    previewImage.style.display = "none";

    uploadedFile.classList.remove("uploaded-file--open");
    uploadedFileInfo.classList.remove("uploaded-file__info--active");

    fileReader.addEventListener("load", function () {
      setTimeout(function () {
        uploadArea.classList.add("upload-area--open");

        loadingText.style.display = "none";
        previewImage.style.display = "block";

        // Add Class (file-details--open) On (fileDetails)
        fileDetails.classList.add("file-details--open");
        // Add Class (uploaded-file--open) On (uploadedFile)
        uploadedFile.classList.add("uploaded-file--open");
        // Add Class (uploaded-file__info--active) On (uploadedFileInfo)
        uploadedFileInfo.classList.add("uploaded-file__info--active");
      }, 500); // 0.5s

      previewImage.setAttribute("src", fileReader.result);

      uploadedFileName.innerHTML = file.name;

      progressMove();
    });

    fileReader.readAsDataURL(file);
  } else {
    // Else

    this;
  }
}

// Progress Counter Increase Function
function progressMove() {
  // Counter Start
  let counter = 0;

  // After 600ms
  setTimeout(() => {
    // Every 100ms
    let counterIncrease = setInterval(() => {
      // If (counter) is equle 100
      if (counter === 100) {
        // Stop (Counter Increase)
        clearInterval(counterIncrease);
      } else {
        // Else
        // plus 10 on counter
        counter = counter + 10;
        // add (counter) vlaue inisde (uploadedFileCounter)
        uploadedFileCounter.innerHTML = `${counter}%`;
      }
    }, 100);
  }, 600);
}

// Simple File Validate Function
function fileValidate(fileType, fileSize) {
  // File Type Validation
  let isImage = imagesTypes.filter(
    (type) => fileType.indexOf(`image/${type}`) !== -1
  );

  // If The Uploaded File Type Is 'jpeg'
  if (isImage[0] === "jpeg") {
    // Add Inisde (uploadedFileIconText) The (jpg) Value
    uploadedFileIconText.innerHTML = "jpg";
  } else {
    // else
    // Add Inisde (uploadedFileIconText) The Uploaded File Type
    uploadedFileIconText.innerHTML = isImage[0];
  }

  // If The Uploaded File Is An Image
  if (isImage.length !== 0) {
    // Check, If File Size Is 2MB or Less
    if (fileSize <= 2000000) {
      // 2MB :)
      return true;
    } else {
      // Else File Size
      return alert("Please Your File Should be 2 Megabytes or Less");
    }
  } else {
    // Else File Type
    return alert("Please make sure to upload An Image File Type");
  }
}

// :)
