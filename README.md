## Setup Guide

1.  Go to your directory where you usually store your PHP projects (e.g. for XAMPP, it's inside `htdocs`).
2.  Clone this repository inside the directory stated above.
    `git clone https://warrenrezane@bitbucket.org/warrenrezane/simple-image-upload.git`
3.  Login to your MySQL Server, and create a database named `simple_image_upload`.
4.  Create a table named `images` with the following data types:
    `id INT NOT NULL PRIMARY_KEY`,
    `filename VARCHAR(191) NOT NULL`,
    `path VARCHAR(191) NOT NULL`,
    `created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP`,
    `updated_at DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP`.
5.  Start your PHP server.
6.  Navigate to the project
    `(e.g. https://localhost/simple-image-upload)`
